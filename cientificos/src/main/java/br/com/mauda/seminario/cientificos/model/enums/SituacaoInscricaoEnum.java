package br.com.mauda.seminario.cientificos.model.enums;

public enum SituacaoInscricaoEnum {

    DISPONIVEL,
    COMPRADO,
    CHECKIN;

    private Long id;
    private String nome;

    public Long getId() {
        return this.id;
    }

    public String getNome() {
        return this.nome;
    }
}