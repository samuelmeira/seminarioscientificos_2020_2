package br.com.mauda.seminario.cientificos.model;

public class Curso {

    private AreaCientifica areaCientifica;

    private Long id;
    private String nome;

    public Curso(AreaCientifica areaCientifica) {
        this.areaCientifica = areaCientifica;
        this.areaCientifica.adicionarCurso(this);
    }

    public AreaCientifica getAreaCientifica() {
        return this.areaCientifica;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
