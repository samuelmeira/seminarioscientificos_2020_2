package br.com.mauda.seminario.cientificos.model;

import java.util.Date;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao {

    private SituacaoInscricaoEnum situacaoInscricaoEnum = SituacaoInscricaoEnum.DISPONIVEL;

    private Long id;
    private Boolean direitoMaterial;
    private Estudante estudante;
    private Seminario seminario;

    public Inscricao(Seminario seminario) {
        this.seminario = seminario;
        seminario.adcionarInscricao(this);
    }

    public void comprar(Estudante estudante, Boolean direitoMaterial) {
        if (SituacaoInscricaoEnum.DISPONIVEL.equals(this.situacaoInscricaoEnum) && new Date().before(this.seminario.getData())) {
            this.situacaoInscricaoEnum = SituacaoInscricaoEnum.COMPRADO;
            this.direitoMaterial = direitoMaterial;
            this.estudante = estudante;
            estudante.adicionarInscricao(this);
        }
    }

    public void cancelarCompra() {
        if (SituacaoInscricaoEnum.COMPRADO.equals(this.situacaoInscricaoEnum) && new Date().before(this.seminario.getData())) {
            this.situacaoInscricaoEnum = SituacaoInscricaoEnum.DISPONIVEL;
            this.estudante.removerInscricao(this);
            this.estudante = null;
            this.direitoMaterial = null;
        }
    }

    public void realizarCheckIn() {
        if (SituacaoInscricaoEnum.COMPRADO.equals(this.situacaoInscricaoEnum) && new Date().before(this.seminario.getData())) {
            this.situacaoInscricaoEnum = SituacaoInscricaoEnum.CHECKIN;
        }
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public void setDireitoMaterial(Boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacaoInscricaoEnum;
    }
}