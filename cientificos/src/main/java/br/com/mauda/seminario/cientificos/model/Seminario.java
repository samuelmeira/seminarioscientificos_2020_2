package br.com.mauda.seminario.cientificos.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Seminario {

    private List<AreaCientifica> areaCientificas = new ArrayList<>();
    private List<Professor> professores = new ArrayList<>();
    private List<Inscricao> inscricoes = new ArrayList<>();

    private Long id;
    private String titulo;
    private String descricao;
    private Boolean mesaRedonda;
    private Date data;
    private Integer qtdInscricoes;

    public Seminario(AreaCientifica areaCientifica, Professor professor, Integer qtdInscricoes) {
        this.adicionarAreaCientifica(areaCientifica);
        this.adicionarProfessor(professor);
        this.qtdInscricoes = qtdInscricoes;

        for (int i = 0; i < qtdInscricoes; i++) {
            new Inscricao(this);
        }
    }

    public void adicionarAreaCientifica(AreaCientifica area) {
        this.areaCientificas.add(area);
    }

    public void adcionarInscricao(Inscricao inscricao) {
        this.inscricoes.add(inscricao);
    }

    public void adicionarProfessor(Professor professor) {
        this.professores.add(professor);
        professor.adicionarSeminario(this);
    }

    public boolean possuiAreaCientifica(AreaCientifica area) {
        return this.areaCientificas.contains(area);
    }

    public boolean possuiInscricao(Inscricao incricao) {
        return this.inscricoes.contains(incricao);
    }

    public boolean possuiProfessor(Professor professor) {
        return this.professores.contains(professor);
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return this.titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescricao() {
        return this.descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Boolean getMesaRedonda() {
        return this.mesaRedonda;
    }

    public void setMesaRedonda(Boolean mesaRedonda) {
        this.mesaRedonda = mesaRedonda;
    }

    public Date getData() {
        return this.data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Integer getQtdInscricoes() {
        return this.qtdInscricoes;
    }

    public void setQtdInscricoes(Integer qtdInscricoes) {
        this.qtdInscricoes = qtdInscricoes;
    }

    public List<AreaCientifica> getAreasCientificas() {
        return this.areaCientificas;
    }

    public List<Professor> getProfessores() {
        return this.professores;
    }

    public List<Inscricao> getInscricoes() {
        return this.inscricoes;
    }
}